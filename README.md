# drone-plugins

[![Build status][ci:badge]][ci:link]

  [ci:badge]: https://img.shields.io/drone/build/algernon/drone-plugins/main?server=https%3A%2F%2Fci.madhouse-project.org&style=for-the-badge
  [ci:link]: https://ci.madhouse-project.org/algernon/drone-plugins

This is a collection of a few [Drone][drone-ci] plugins I created, mostly for personal use. For more information about them, check their respective documentations:

- [drone-plugin-dco][plugin:dco]
- [drone-plugin-signature-check][plugin:signature-check]
- [drone-plugin-minio][plugin:minio]

 [drone-ci]: https://drone.io/
 [plugin:dco]: plugins/dco/README.md
 [plugin:signature-check]: plugins/signature-check/README.md
 [plugin:minio]: plugins/minio/README.md
