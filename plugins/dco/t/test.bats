#! /usr/bin/env bats
## dco-check -- DCO enforcement tool
## Copyright (C) 2018, 2019, 2020  Gergely Nagy
##
## This program is free software: you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation, either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
## FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along with
## this program. If not, see <http://www.gnu.org/licenses/>.

REPODIR="/tmp/dco-test-$$.repo"
DCO="$(pwd)/bin/dco-check"

new_commit() {
    msg="$1"
    shift
    git commit --quiet --allow-empty -m "${msg}" $@
}

setup() {
    install -d "${REPODIR}"
    cd "${REPODIR}"

    git init --quiet
    git config user.name "BATS"
    git config user.email "bats@local"

    git remote add origin .

    new_commit "Initial import" -s

    export DRONE_REPO_SCM=git
    export DRONE_REPO_BRANCH=master
    export DRONE_PREV_COMMIT_SHA=
    export DRONE_COMMIT=
}

teardown() {
    rm -rf "${REPODIR}"
}

@test "success: DCO of initial commit" {
    cd "${REPODIR}"

    export DRONE_COMMIT="$(git show --quiet --format=format:%H)"

    run ${DCO}

    [ "$status" -eq 0 ]
    [ "$output" = "✓ Correct Developer Certificate of Origin." ]
}

@test "success: DCO on a single commit" {
    cd "${REPODIR}"

    git checkout --quiet -b f/new-branch
    new_commit "Another commit" -s

    export DRONE_COMMIT="$(git show --quiet --format=format:%H)"

    run ${DCO}

    [ "$status" -eq 0 ]
    [ "$output" = "✓ Correct Developer Certificate of Origin." ]
}

@test "success: DCO on multiple commits" {
    cd "${REPODIR}"

    git checkout --quiet -b f/new-branch
    new_commit "." -s
    new_commit "." -s

    export DRONE_COMMIT="$(git show --quiet --format=format:%H)"

    run ${DCO}

    [ "$status" -eq 0 ]
    [ "$output" = "✓ Correct Developer Certificate of Origin." ]
}

@test "success: direct commit to master" {
    cd "${REPODIR}"

    new_commit "." -s

    export DRONE_PREV_COMMIT_SHA="$(git show --quiet --format=format:%H HEAD~1)"
    export DRONE_COMMIT="$(git show --quiet --format=format:%H)"

    run ${DCO}

    [ "$status" -eq 0 ]
    [ "$output" = "✓ Correct Developer Certificate of Origin." ]
}

@test "success: no DCO on a merge commit" {
    cd "${REPODIR}"

    git checkout --quiet -b f/new-branch
    new_commit "." -s
    git checkout --quiet master
    git merge --quiet --no-ff --no-edit f/new-branch

    export DRONE_PREV_COMMIT_SHA="$(git show --quiet --format=format:%H HEAD^1)"
    export DRONE_COMMIT="$(git show --quiet --format=format:%H)"

    run ${DCO}

    [ "$status" -eq 0 ]
    [ "$output" = "✓ Correct Developer Certificate of Origin." ]
}

@test "success: no DCO on a merge commit, topmost only" {
    cd "${REPODIR}"

    git checkout --quiet -b f/new-branch
    new_commit "." -s
    git checkout --quiet master
    git merge --quiet --no-ff --no-edit f/new-branch

    export DRONE_COMMIT="$(git show --quiet --format=format:%H)"

    run ${DCO}

    [ "$status" -eq 0 ]
    echo "$output" | grep -q "Merge commits like [0-9a-f]* are allowed to not have sign-offs"
}

@test "success: no DCO on a tag" {
    cd "${REPODIR}"

    git checkout --quiet -b f/no-dco-tag
    new_commit "." -s
    cat >MSG <<EOF
Test tag.
EOF
    git tag -F MSG no-dco-tag

    export DRONE_COMMIT="$(git show --quiet --format=format:%H)"
    export DRONE_TAG="no-dco-tag"
    run ${DCO}

    [ "$status" -eq 0 ]
    echo "$output" | grep -q "Tags like ${DRONE_TAG} are allowed to not have sign-offs"
}

@test "success: DCO on an a tag" {
    cd "${REPODIR}"

    git checkout --quiet -b f/dco-tag
    new_commit "." -s
    cat >MSG <<EOF
Test tag.

Signed-Off-By: BATS <bats@local>
EOF
    git tag -F MSG dco-tag

    export DRONE_COMMIT="$(git show --quiet --format=format:%H)"
    export DRONE_TAG="dco-tag"
    run ${DCO}

    [ "$status" -eq 0 ]
    echo "$output" | grep -q "Tags like ${DRONE_TAG} are allowed to not have sign-offs"
    echo "$output" | grep -q "✓ Correct Developer Certificate of Origin."
}

@test "success: no DCO on an unannotated tag" {
    cd "${REPODIR}"

    git checkout --quiet -b f/simple-tag
    new_commit "." -s
    git tag simple-tag

    export DRONE_COMMIT="$(git show --quiet --format=format:%H)"
    export DRONE_TAG="simple-tag"
    run ${DCO}

    [ "$status" -eq 0 ]
    echo "$output" | grep -q "Tags like ${DRONE_TAG} are allowed to not have sign-offs"
}

@test "success: DCO, force push" {
    cd "${REPODIR}"

    new_commit "." -s

    export DRONE_COMMIT="$(git show --quiet --format=format:%H HEAD~1)"
    export DRONE_REPO_BRANCH="${DRONE_COMMIT}"
    export DRONE_PREV_COMMIT_SHA="$(git show --quiet --format=format:%H HEAD)"

    run ${DCO}

    [ "$status" -eq 0 ]
    [ "$output" = "✓ Correct Developer Certificate of Origin." ]
}

@test "fail: no commits" {
    cd "${REPODIR}"

    run ${DCO}

    [ "$status" -eq 2 ]
    [ "$output" = "⚠ Could not determine the list of commits for the build." ]
}

@test "fail: lacking DCO" {
    cd "${REPODIR}"

    git checkout --quiet -b f/new-branch
    new_commit "."

    export DRONE_COMMIT="$(git show --quiet --format=format:%H)"

    run ${DCO}

    [ "$status" -eq 1 ]
    echo "$output" | grep -q "does not have any sign-offs"
}

@test "fail: wrong DCO" {
    cd "${REPODIR}"

    git checkout --quiet -b f/new-branch
    new_commit ".

Signed-off-by: Someone Else <else@other>"

    export DRONE_COMMIT="$(git show --quiet --format=format:%H)"

    run ${DCO}

    [ "$status" -eq 1 ]
    echo "$output" | grep -q "does not have a sign-off from its author"
}

@test "fail: wrong SCM" {
    export DRONE_REPO_SCM=rcs
    export DRONE_GIT_SSH_URL=""

    run ${DCO}

    [ "$status" -eq 2 ]
    echo "$output" | grep -q "This plugin only supports git repositories"
}

@test "success: drone 1.0 lacking DRONE_REPO_SCM" {
    export DRONE_REPO_SCM=""
    export DRONE_GIT_SSH_URL="..."

    export DRONE_COMMIT="$(git show --quiet --format=format:%H)"

    run ${DCO}

    [ "$status" -eq 0 ]
    [ "$output" = "✓ Correct Developer Certificate of Origin." ]
}
