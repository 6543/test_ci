# drone-plugin-signature-check

[![Build Status](https://ci.madhouse-project.org/api/badges/algernon/drone-plugins/status.svg?branch=main)](https://ci.madhouse-project.org/algernon/drone-plugins)

This [Drone CI][drone] plugin enforces that all tags are GPG signed. If given a
list of keys, the plugin will attempt to verify signatures, with the given keys
imported first (and only those).

 [drone]: https://drone.io/

The plugin only works with `git`-based repositories at the moment.

## Usage

```yaml
kind: pipeline
name: default

steps:
  - name: signature-verify
    image: algernon/drone-plugin-signature-check
    when:
      event: tag
```

To enable verification of signatures, a list of keys should be given:

```yaml
kind: pipeline
name: default

steps:
  - name: signature-verify
    image: algernon/drone-plugin-signature-check
    settings:
      keys: [ c0ffee, 1234 ]
    when:
      event: tag
```

One can also control which keyserver to pull the keys from:

```yaml
kind: pipeline
name: default

steps:
  - name: signature-verify
    image: algernon/drone-plugin-signature-check
    settings:
      keys: [ c0ffee, 1234 ]
      keyserver: hkp://keys.gnupg.net
    when:
      event: tag
```

If, for some reason the plugin misbehaves, or fails with an error, one can turn
on debugging before reporting the problem:

```yaml
kind: pipeline
name: default

steps:
  - name: signature-verify
    image: algernon/drone-plugin-signature-check
    settings:
      debug: true
    when:
      event: tag
```
