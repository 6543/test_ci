#! /usr/bin/env bats
## signature-check -- GPG signature enforcement tool
## Copyright (C) 2018  Gergely Nagy
##
## This program is free software: you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation, either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
## FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along with
## this program. If not, see <http://www.gnu.org/licenses/>.

TESTDIR="/tmp/signature-check-$$"
REPODIR="${TESTDIR}/repo"
GNUPGHOME="${TESTDIR}/gnupg"
export GNUPGHOME

SC="$(pwd)/bin/signature-check"

setup() {
    install -d "${REPODIR}" "${GNUPGHOME}"
    chmod 0700 "${GNUPGHOME}"

    cat >"${GNUPGHOME}/key-desc.batch" <<EOF
%echo Generating a default key
%no-protection
%transient-key

Key-Type: default
Subkey-Type: default
Name-Real: BATS
Name-Email: bats@local
Expire-Date: 0
%commit
%echo done
EOF

    gpg --batch --generate-key "${GNUPGHOME}/key-desc.batch"

    gpg --list-secret-keys &>/dev/null

    cd "${REPODIR}"

    git init --quiet
    git config user.name "BATS"
    git config user.email "bats@local"
    git config user.signingkey "bats@local"

    git commit --allow-empty -m "Initial import"

    export DRONE_REPO_SCM=git
    export DRONE_BUILD_EVENT=tag
    export DRONE_TAG=
}

teardown() {
    rm -rf "${TESTDIR}"
}

@test "skip: not a tag event" {
    export DRONE_BUILD_EVENT=commit

    run ${SC}
    [ "$status" -eq 0 ]
    [ "$output" = "- SKIP: Not a tag event, skipping." ]
}

@test "fail: unannotated tag" {
    cd "${REPODIR}"

    git tag test-tag
    export DRONE_TAG=test-tag

    run ${SC}
    [ "$status" -eq 1 ]
    [ "$output" = "✗ ERROR: \`test-tag' is not an annotated, signed tag." ]
}

@test "fail: annotated, unsigned tag" {
    cd "${REPODIR}"

    git tag -a -m "Test tag" test-tag
    export DRONE_TAG=test-tag

    run ${SC}

    [ "$status" -eq 1 ]
    [ "$output" = "✗ ERROR: \`test-tag' is annotated, but unsigned." ]
}

@test "fail: signed tag, unverified (w/ verification required)" {
    cd "${REPODIR}"

    git tag -s -m "Test tag" test-tag
    export DRONE_TAG=test-tag
    export PLUGIN_KEYS="$(gpg --list-keys --with-colons | grep '^pub' | cut -d: -f5)"
    rm -rf "${GNUPGHOME}"

    run ${SC}

    [ "$status" -eq 1 ]
    echo "$output" | grep -q "✗ \`test-tag' is signed, but could not be verified."
}

@test "success: signed tag, unverified" {
    cd "${REPODIR}"

    git tag -s -m "Test tag" test-tag
    export DRONE_TAG=test-tag
    rm -rf "${GNUPGHOME}"

    run ${SC}

    [ "$status" -eq 0 ]
    [ "$output" = "✓ \`test-tag' is properly signed." ]
}

@test "success: signed tag, verified" {
    cd "${REPODIR}"

    git tag -s -m "Test tag" test-tag
    export DRONE_TAG=test-tag
    export PLUGIN_KEYS="$(gpg --list-keys --with-colons | grep '^pub' | cut -d: -f5)"

    run ${SC}

    [ "$status" -eq 0 ]
    [ "$output" = "✓ \`test-tag' is properly signed, and verified." ]
}
