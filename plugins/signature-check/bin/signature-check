#! /bin/sh
## signature-check -- GPG signature enforcement tool
## Copyright (C) 2018, 2019, 2022  Gergely Nagy
##
## This program is free software: you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation, either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
## FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along with
## this program. If not, see <http://www.gnu.org/licenses/>.

set -e

case "${PLUGIN_DEBUG}" in
    [tT][rR][uU][eE]|[yY][eE][sS]|[yY]|1)
        set -x
        ;;
esac

verify_tag_signature() {
    if [ "${DRONE_BUILD_EVENT}" != "tag" ]; then
        echo "- SKIP: Not a tag event, skipping." >&2
        return 0
    fi

    VERIFICATION="$(git verify-tag --raw "${DRONE_TAG}" 2>&1 || true)"

    if echo "${VERIFICATION}" | grep -q "cannot verify a non-tag object of type commit"; then
       echo "✗ ERROR: \`${DRONE_TAG}' is not an annotated, signed tag." >&2
        return 1
    fi

    if echo "${VERIFICATION}" | grep -q "\[GNUPG:\] NODATA" ||
       echo "${VERIFICATION}" | grep -q "error: no signature found"; then
        echo "✗ ERROR: \`${DRONE_TAG}' is annotated, but unsigned." >&2
        return 1
    fi

    if echo "${VERIFICATION}" | grep -q "\[GNUPG:\] NO_PUBKEY" ||
       echo "${VERIFICATION}" | grep -q "\[GNUPG:\] GOODSIG"; then

        if [ -n "${PLUGIN_KEYS}" ]; then
            if ! echo "${VERIFICATION}" | grep -q "\[GNUPG:\] GOODSIG"; then
                echo "✗ \`${DRONE_TAG}' is signed, but could not be verified." >&2
                return 1
            else
                echo "✓ \`${DRONE_TAG}' is properly signed, and verified."
                return 0
            fi
        else
            echo "✓ \`${DRONE_TAG}' is properly signed."
            return 0
        fi
    fi

    echo "✗ ERROR: \`${DRONE_TAG}' could not be verified." >&2

    return 1
}

if [ "${DRONE_REPO_SCM}" != "git" ] && [ -z "${DRONE_GIT_SSH_URL}" ]; then
    echo "⚠ This plugin only supports git repositories, ${DRONE_REPO_SCM} is not supported yet." >&2
    echo "Please disable the plugin, or request your SCM to be supported." >&2
    echo "You can file issues at:" >&2
    echo "  https://git.madhouse-project.org/algernon/drone-plugins/issues." >&2
    exit 2
fi

if [ -n "${PLUGIN_KEYS}" ]; then
    IFS_SAVE="${IFS}"
    IFS=","
    for key in ${PLUGIN_KEYS}; do
        if ! gpg --with-colons --list-keys "${key}" >/dev/null; then
            if [ -z "${PLUGIN_KEYSERVER}" ]; then
                gpg --keyserver hkps://keys.openpgp.org --recv-keys "${key}" || true
            else
                gpg --keyserver "${PLUGIN_KEYSERVER}" --recv-keys "${key}" || true
            fi
        fi
    done
    IFS="${IFS_SAVE}"
fi

verify_tag_signature
